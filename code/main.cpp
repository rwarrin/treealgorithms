#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

struct node
{
	int32_t Value;
	struct node *Left;
	struct node *Right;
};

static inline node *
TreeInsert(struct node *Tree, int32_t Value)
{
	struct node *NewNode = 0;
	NewNode = (struct node *)malloc(sizeof(struct node));
	if(NewNode != 0)
	{
		NewNode->Value = Value;
		NewNode->Left = 0;
		NewNode->Right = 0;

		if(Tree != 0)
		{
			struct node *TreeWalker = Tree;
			while(TreeWalker != 0)
			{
				if(Value == TreeWalker->Value)
				{
					free(NewNode);
					NewNode = TreeWalker;
					break;
				}
				else if(Value < TreeWalker->Value)
				{
					if(TreeWalker->Left == 0)
					{
						TreeWalker->Left = NewNode;
						break;
					}
					else
					{
						TreeWalker = TreeWalker->Left;
					}
				}
				else if(Value > TreeWalker->Value)
				{
					if(TreeWalker->Right == 0)
					{
						TreeWalker->Right = NewNode;
						break;
					}
					else
					{
						TreeWalker = TreeWalker->Right;
					}
				}
			}
		}
	}

	return NewNode;
}

static inline void
RightRotate(struct node **Tree)
{
	struct node *NewRoot = (*Tree)->Left;
	if(NewRoot != 0)
	{
		struct node *DetachedBranch = NewRoot->Right;

		NewRoot->Right = *Tree;
		(*Tree)->Left = DetachedBranch;
		(*Tree) = NewRoot;
	}
}

static inline void
LeftRotate(struct node **Tree)
{
	struct node *NewRoot = (*Tree)->Right;
	if(NewRoot != 0)
	{
		struct node *DetachedBranch = NewRoot->Left;

		NewRoot->Left = *Tree;
		(*Tree)->Right = DetachedBranch;
		(*Tree) = NewRoot;
	}
}

static inline void
TreeToBackbone(struct node **Tree)
{
	// NOTE(rick): This can also be accomplished using in-order traversal
	// building a linked list by chaining right.

	int32_t NewRootFound = 0;
	struct node *PreviousRightParent = 0;
	struct node *TreeWalker = *Tree;

	while(TreeWalker != 0)
	{
		while(TreeWalker->Left != 0)
		{
			RightRotate(&TreeWalker);

			if(PreviousRightParent != 0)
			{
				PreviousRightParent->Right = TreeWalker;
			}
		}

		if(NewRootFound == 0)
		{
			NewRootFound = 1;
			*Tree = TreeWalker;
		}

		PreviousRightParent = TreeWalker;
		TreeWalker = TreeWalker->Right;
	}
}

static inline int32_t
GetTreeNodeCount(struct node *Tree)
{
	if(Tree == 0)
	{
		return 0;
	}

	int Count = 1;
	Count += GetTreeNodeCount(Tree->Left);
	Count += GetTreeNodeCount(Tree->Right);

	return Count;
}

static inline void
RebalanceTreeWithDSW(struct node **Tree)
{
	// TODO(rick): I can probably compress this code..
	int32_t NodeCount = GetTreeNodeCount(*Tree);
	int32_t Leaves = (NodeCount + 1) - (1 << (int32_t)log2(NodeCount + 1));

	struct node *PreviousRotatedNode = 0;
	struct node *LeafRotater = *Tree;
	for(int32_t LeafRotations = 0; LeafRotations < Leaves; ++LeafRotations)
	{
		LeftRotate(&LeafRotater);

		if(LeafRotations == 0)
		{
			*Tree = LeafRotater;
		}

		if(PreviousRotatedNode)
		{
			PreviousRotatedNode->Right = LeafRotater;
		}

		PreviousRotatedNode = LeafRotater;
		LeafRotater = LeafRotater->Right;
	}

	NodeCount = NodeCount - Leaves;
	while(NodeCount > 1)
	{
		struct node *PreviousRotatedNode = 0;
		struct node *Rotator = *Tree;
		for(int32_t Rotations = 0; Rotations < NodeCount / 2; ++Rotations)
		{
			LeftRotate(&Rotator);
			if(Rotations == 0)
			{
				*Tree = Rotator;
			}

			if(PreviousRotatedNode)
			{
				PreviousRotatedNode->Right = Rotator;
			}

			PreviousRotatedNode = Rotator;
			Rotator = Rotator->Right;
		}

		NodeCount >>= 1;
	}
}

static void
PrintInOrder(struct node *Tree, int32_t Depth = 0)
{
	if(Tree == 0)
	{
		return;
	}

	PrintInOrder(Tree->Left, Depth + 1);
	printf("%d(%d), ", Tree->Value, Depth);
	PrintInOrder(Tree->Right, Depth + 1);
}

int main(void)
{
	struct node *Tree = 0;
	int32_t NodesToAdd = 50;
	for(int NodeCount = 0; NodeCount < NodesToAdd; ++NodeCount)
	{
		int32_t Value = rand() % (NodesToAdd * 2);
		struct node *NewNode = TreeInsert(Tree, Value);
		if(Tree == 0)
		{
			Tree = NewNode;
		}
	}

	TreeToBackbone(&Tree);
	PrintInOrder(Tree);
	printf("\n");

	RebalanceTreeWithDSW(&Tree);
	PrintInOrder(Tree);
	printf("\n");
	return 0;
}
